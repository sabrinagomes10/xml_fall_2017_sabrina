<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:math="http://www.w3.org/2005/xpath-functions/math" xmlns:array="http://www.w3.org/2005/xpath-functions/array" xmlns:map="http://www.w3.org/2005/xpath-functions/map" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:err="http://www.w3.org/2005/xqt-errors" exclude-result-prefixes="array fn map math xhtml xs err" version="3.0">
	<xsl:output method="html" version="5.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/" name="xsl:initial-template">
	<!-- Great work, Sabrina.  Your XSL properly formats andn outputs the xml data.  Nicely done
10/10
-->
	<html>
		<head>
			<title>Telephone Bill</title>
		</head>
		<body>
			Customer Info<br/><br/>
				Name: <xsl:value-of select="telephoneBill/customer/name"/>
				<br/><br/>
				Address: <xsl:value-of select="telephoneBill/customer/address"/>
				<br/><br/>
				City: <xsl:value-of select="telephoneBill/customer/city"/>
				<br/><br/>
				Province: <xsl:value-of select="telephoneBill/customer/province"/>
				<br/><br/>
				<table border="1">
					<tbody>
					<tr>
							<th>Called Number</th>
							<th>Date</th>
							<th>Duration in minutes</th>
							<th>Charge</th>
						</tr>
					<xsl:for-each select="telephoneBill/call">
						<tr>
							<td>
								<xsl:value-of select="@number"/>
							</td>
							<td>
								<xsl:value-of select="@date"/>
							</td>
							<td>
								<xsl:value-of select="@durationInMinutes"/>
							</td>
							<td>
								<xsl:value-of select="@charge"/>
							</td>
						</tr>
					</xsl:for-each>
					</tbody>
				</table>				
		</body>
	</html>
	</xsl:template>
</xsl:stylesheet>
